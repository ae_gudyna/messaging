﻿using QueuesInfrastucture;
using System;
using System.IO;
using System.Messaging;
using System.Threading;

namespace WindowsServiceServer
{
    class Listener
    {
        private string folderForListener = ApplicationSettings.RootFolder+"\\Server";
        private bool listenQueue;
        private readonly object obj = new object();
        private readonly string queueName = ".\\Private$\\TransferFilesQueue";
        private readonly MessageQueue queue;

        public Listener()
        {
            if (!Directory.Exists(folderForListener))
            {
                Directory.CreateDirectory(folderForListener);
            }

            queue = !MessageQueue.Exists(queueName) ? MessageQueue.Create(queueName) : new MessageQueue(queueName);

            queue.Formatter = new BinaryMessageFormatter();
        }

        public void Start()
        {
            listenQueue = true;

            while (listenQueue)
            {
                Thread.Sleep(2000);

                using (queue)
                {
                    var msg = queue.Receive();

                    RecordEntry((MessageForReceiving)msg.Body);
                }
            }
        }

        public void Stop()
        {
            listenQueue = false;
        }

        private void RecordEntry(MessageForReceiving msg)
        {
            lock (obj)
            {
                File.WriteAllBytes(folderForListener + "\\" + msg.FileName, msg.Content);
                using (StreamWriter writer = new StreamWriter(folderForListener + "\\log.txt", true))
                {
                    writer.WriteLine($"({DateTime.Now:dd / MM / yyyy hh: mm:ss}) receive: {msg.FileName}");
                    writer.Flush();
                }
            }
        }
    }
}
