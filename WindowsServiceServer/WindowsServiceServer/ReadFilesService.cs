﻿using System.IO;
using System.ServiceProcess;
using System.Threading;

namespace WindowsServiceServer
{
    public partial class ReadFilesService : ServiceBase
    {
        Listener listener;
        SettingsSender sender;

        public ReadFilesService()
        {
            InitializeComponent();

            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
        }

        protected override void OnStart(string[] args)
        {
            listener = new Listener();
            sender = new SettingsSender();
            if (!Directory.Exists(ApplicationSettings.RootFolder))
            {
                Directory.CreateDirectory(ApplicationSettings.RootFolder);
            }
            Thread listenerThread = new Thread(new ThreadStart(listener.Start));
            Thread settingsSenderThread = new Thread(new ThreadStart(sender.Start));

            listenerThread.Start();
            settingsSenderThread.Start();
        }

        protected override void OnStop()
        {
            listener.Stop();
            sender.Stop();
            Thread.Sleep(1000);
        }
    }
}
