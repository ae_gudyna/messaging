﻿using System.IO;
using System.Messaging;
using QueuesInfrastucture;

namespace WindowsServiceServer
{
    public class SettingsSender
    {
        private readonly string folderForSettings;
        private readonly string fileName;
        private readonly MessageQueue settingsQueue;
        private readonly string settingsQueueName = ".\\Private$\\SettingsQueue";
        private readonly FileSystemWatcher watcher;

        public SettingsSender()
        {
            folderForSettings = ApplicationSettings.RootFolder + "\\Settings";
            fileName = folderForSettings + "\\settings.xml";
            
            if (!Directory.Exists(folderForSettings))
            {
                Directory.CreateDirectory(folderForSettings);
            }

            settingsQueue = !MessageQueue.Exists(settingsQueueName) ? MessageQueue.Create(settingsQueueName) : new MessageQueue(settingsQueueName);
            watcher = new FileSystemWatcher(folderForSettings);

            watcher.Created += ReadSettings;
            watcher.Changed += ReadSettings;
            //settingsQueue.Formatter = new BinaryMessageFormatter();
        }
        public void Start()
        {
            watcher.EnableRaisingEvents = true;

            if (!File.Exists(fileName))
            {
                SettingsHelper.CreateSettingsFile(fileName);
            }
        }

        public void Stop()
        {
            watcher.EnableRaisingEvents = false;
        }

        private void ReadSettings(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath != fileName) return;

            var settings = SettingsHelper.GetSettings(fileName);

            using (settingsQueue)
            {
                settingsQueue.Send(settings);
            }
        }
    }
}
