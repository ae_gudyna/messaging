﻿using System.ComponentModel;
using System.ServiceProcess;

namespace WindowsServiceServer
{
    [RunInstaller(true)]
    public partial class ReadFileInstaller : System.Configuration.Install.Installer
    {
        ServiceInstaller serviceInstaller;
        ServiceProcessInstaller processInstaller;

        public ReadFileInstaller()
        {
            InitializeComponent();
            serviceInstaller = new ServiceInstaller();
            processInstaller = new ServiceProcessInstaller();

            processInstaller.Account = ServiceAccount.LocalSystem;
            serviceInstaller.StartType = ServiceStartMode.Manual;
            serviceInstaller.ServiceName = nameof(ReadFilesService);
            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}
