﻿using QueuesInfrastucture;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WindowsServiceServer
{
    class ClientSettings : Settings
    {
        public static Settings Settings;
        private static string path = "C:\\temp\\Settings.xml";
        private ClientSettings(Dictionary<string, string> settingsList)
        {
            if (settingsList.TryGetValue("readFilesTimeOut", out var readFilesTimeOut))
            {
                ReadFilesTimeOut = readFilesTimeOut;
            }
            if (settingsList.TryGetValue("refreshStatusTimeOut", out var refreshStatusTimeOut))
            {
                RefreshStatusTimeOut = refreshStatusTimeOut;
            }
        }

        private static Dictionary<string, string> GetSettingsList()
        {
            Dictionary<string, string> _ret = new Dictionary<string, string>();
            if (File.Exists(path))
            {
                StreamReader reader = new StreamReader
                (
                    new FileStream(
                        path,
                        FileMode.Open,
                        FileAccess.Read,
                        FileShare.Read)
                );
                XmlDocument doc = new XmlDocument();
                string xmlIn = reader.ReadToEnd();
                reader.Close();
                doc.LoadXml(xmlIn);
                foreach (XmlNode child in doc.ChildNodes)
                    if (child.Name.Equals("Settings"))
                        foreach (XmlNode node in child.ChildNodes)
                            if (node.Name.Equals("add"))
                                _ret.Add
                                (
                                    node.Attributes["key"].Value,
                                    node.Attributes["value"].Value
                                );
            }
            return _ret;
        }
        public static Settings GetSettings()
        {
            if (Settings == null)
            {
                return new ClientSettings(GetSettingsList());
            }
            return Settings;
        }

        public bool SendNewClientSettings()
        {
            var settings = GetSettingsList();
            var needSend = false;
            if (settings.TryGetValue("readFilesTimeOut", out var readFilesTimeOut) && Settings.ReadFilesTimeOut != readFilesTimeOut)
            {
                Settings.ReadFilesTimeOut = readFilesTimeOut;
                needSend = true;
            }
            if (settings.TryGetValue("refreshStatusTimeOut", out var refreshStatusTimeOut) && Settings.RefreshStatusTimeOut != refreshStatusTimeOut)
            {
                Settings.RefreshStatusTimeOut = refreshStatusTimeOut;
                needSend = true;
            }
            return needSend;
        }
    }
}
