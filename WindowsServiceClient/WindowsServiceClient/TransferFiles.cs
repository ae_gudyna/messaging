﻿using QueuesInfrastucture;
using System;
using System.IO;
using System.Messaging;
using System.Threading;

namespace WindowsServiceClient
{
    public class TransferFiles
    {
        private readonly FileSystemWatcher watcher;
        private readonly object obj = new object();
        private readonly string folderForFiles;
        private readonly string folderForLogs;
        private readonly string queueName = ".\\Private$\\TransferFilesQueue";
        private MessageQueue queue;
        private bool enabled = true;
        private int messageNumber = 0;

        private MessageQueue settingsQueue;
        private string settingsQueueName = ".\\Private$\\SettingsQueue";
        private Settings ClientSettings = new Settings { ReadFilesTimeOut = 1000, RefreshStatusTimeOut = 1000};

        public TransferFiles()
        {
            folderForFiles = ApplicationSettings.RootFolder + "\\Client";
            folderForLogs = folderForFiles + "\\logs";

            if (!Directory.Exists(folderForFiles))
            {
                Directory.CreateDirectory(folderForFiles);
            }

            if (!Directory.Exists(folderForLogs))
            {
                Directory.CreateDirectory(folderForLogs);
            }
            
            watcher = new FileSystemWatcher(folderForFiles);

            CheckIfExistQueue();
            CheckIfExistSettingsQueue();
        }

        private void CheckIfExistQueue()
        {
            if (MessageQueue.Exists(queueName))
            {
                queue = new MessageQueue(queueName);
                queue.Formatter = new BinaryMessageFormatter();
                watcher.Created += Watcher_Created;
            }
        }

        private void CheckIfExistSettingsQueue()
        {
            if (MessageQueue.Exists(settingsQueueName))
            {
                settingsQueue = new MessageQueue(settingsQueueName)
                {
                    Formatter = new XmlMessageFormatter(new Type[] { typeof(Settings) })
                };
                settingsQueue.PeekCompleted += OnMessageAdded;
                settingsQueue.BeginPeek(new TimeSpan(0, 1, 0), messageNumber++);
            }
        }

        private void OnMessageAdded(object source, PeekCompletedEventArgs asyncResult)
        {
            var mq = (MessageQueue)source;

            var m = mq.EndPeek(asyncResult.AsyncResult);
            ClientSettings = (Settings) m.Body;
            try
            {
                mq.Receive();
            }
            catch (Exception ex)
            {
                RecordExeption(ex.Message);
            }
            mq.BeginPeek(new TimeSpan(0, 1, 0), messageNumber++);
        }

        public void Start()
        {
            watcher.EnableRaisingEvents = true;

            while (enabled)
            {
                Thread.Sleep(ClientSettings.ReadFilesTimeOut);

                if(queue == null)
                {
                    CheckIfExistQueue();
                }
                if(settingsQueue == null)
                {
                    CheckIfExistSettingsQueue();
                }
            }
        }
        public void Stop()
        {
            watcher.EnableRaisingEvents = false;
            enabled = false;
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            string fileEvent = "Add";
            var fileName = e.Name;
            RecordEntry(fileEvent, fileName);
            try
            {
                using (queue)
                {
                    var file = File.ReadAllBytes(e.FullPath);
                    var message = new MessageForReceiving { FileName = e.Name, Content = file };

                    queue.Send(new Message(message, new BinaryMessageFormatter()));

                    File.Delete(e.FullPath);
                }
            }
            catch(Exception ex)
            {
                RecordExeption(ex.Message);
            }
        }

        private void RecordEntry(string fileEvent, string fileName)
        {
            lock (obj)
            {
                using (StreamWriter writer = new StreamWriter(folderForLogs + "\\log.txt", true))
                {
                    writer.WriteLine($"{DateTime.Now:dd/MM/yyyy hh:mm:ss} {fileEvent}: {fileName}");
                    writer.Flush();
                }
            }
        }

        private void RecordExeption(string exception)
        {
            lock (obj)
            {
                using (StreamWriter writer = new StreamWriter(folderForLogs + "\\log.txt", true))
                {
                    writer.WriteLine($"({DateTime.Now:dd/MM/yyyy hh:mm:ss}) [EXCEPTION]: {exception}");
                    writer.Flush();
                }
            }
        }
    }
}
