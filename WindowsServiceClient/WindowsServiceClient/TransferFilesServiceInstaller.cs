﻿using System.ComponentModel;
using System.ServiceProcess;

namespace WindowsServiceClient
{
    [RunInstaller(true)]
    public partial class TransferFilesServiceInstaller : System.Configuration.Install.Installer
    {
        ServiceInstaller serviceInstaller;
        ServiceProcessInstaller processInstaller;

        public TransferFilesServiceInstaller()
        {
            InitializeComponent();
            serviceInstaller = new ServiceInstaller();
            processInstaller = new ServiceProcessInstaller();

            processInstaller.Account = ServiceAccount.LocalSystem;
            serviceInstaller.StartType = ServiceStartMode.Manual;
            serviceInstaller.ServiceName = nameof(TransferFilesService);
            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}
