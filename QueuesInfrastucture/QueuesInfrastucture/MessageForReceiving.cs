﻿using System;

namespace QueuesInfrastucture
{
    [Serializable]
    public class MessageForReceiving
    {
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        public string ClientName { get; set; }
    }
}
