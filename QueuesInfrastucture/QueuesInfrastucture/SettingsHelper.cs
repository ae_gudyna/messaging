﻿using System.IO;
using System.Xml;

namespace QueuesInfrastucture
{
    public static class SettingsHelper
    {
        public static void CreateSettingsFile(string fileName)
        {
            var settings = new Settings { ReadFilesTimeOut = 2000, RefreshStatusTimeOut = 2000 };
            CreateSettingsFile(fileName, settings);
        }

        public static void CreateSettingsFile(string fileName, Settings settings)
        {
            var doc = new XmlDocument();
            var docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            var settingsNode = doc.CreateElement("Settings");
            doc.AppendChild(settingsNode);

            XmlNode ReadFilesTimeOutNode = doc.CreateElement("ReadFilesTimeOut");
            XmlAttribute valueAttribute = doc.CreateAttribute("value");
            valueAttribute.Value = settings.ReadFilesTimeOut.ToString();
            ReadFilesTimeOutNode.Attributes.Append(valueAttribute);
            settingsNode.AppendChild(ReadFilesTimeOutNode);

            XmlNode RefreshStatusTimeOutNode = doc.CreateElement("RefreshStatusTimeOut");
            valueAttribute = doc.CreateAttribute("value");
            valueAttribute.Value = settings.RefreshStatusTimeOut.ToString();
            RefreshStatusTimeOutNode.Attributes.Append(valueAttribute);
            settingsNode.AppendChild(RefreshStatusTimeOutNode);

            doc.Save(fileName);
        }

        public static Settings GetSettings(string fileName)
        {
            var settings = new Settings();
            StreamReader reader = new StreamReader
            (
                new FileStream(
                    fileName,
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.Read)
            );
            XmlDocument doc = new XmlDocument();
            string xmlIn = reader.ReadToEnd();
            reader.Close();
            doc.LoadXml(xmlIn);
            foreach (XmlNode child in doc.ChildNodes)
                if (child.Name.Equals("Settings"))
                    foreach (XmlNode node in child.ChildNodes)
                    {
                        if (node.Name.Equals("ReadFilesTimeOut"))
                        {
                            settings.ReadFilesTimeOut = int.Parse(node.Attributes[0].Value);
                        }
                        if (node.Name.Equals("RefreshStatusTimeOut"))
                        {
                            settings.RefreshStatusTimeOut = int.Parse(node.Attributes[0].Value);
                        }
                    }
            return settings;
        }
    }
}
