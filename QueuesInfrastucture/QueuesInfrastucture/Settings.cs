﻿using System;

namespace QueuesInfrastucture
{
    [Serializable]
    public class Settings
    {
        public int ReadFilesTimeOut { get; set; }
        public int RefreshStatusTimeOut { get; set; }
    }
}
